### How this task will be evaluated
### We expect your solution to the task to meet the following criteria:

* In addition to the main tags, the markup should contain the following tags: <a>, <ul>, <li>, <h1>, <h2>, <h3>, <p>, <button>, <i>, <img>, <br>
* You have created a top menu using the <ul>, <li>, <a> tags
* The markup which you have created should contain two different logo images: small and big
* You have divided headings using the <br> tag
* You have created the bottom button that contains ▶ symbol &#9654